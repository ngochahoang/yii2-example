<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%point}}`.
 */
class m190903_014835_drop_point_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('point');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%point}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}

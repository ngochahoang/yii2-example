<?php

use yii\db\Migration;

/**
 * Class m190826_174010_add_point_table
 */
class m190826_174010_add_point_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('point', [
            'id' => $this->primaryKey(),
            'x' => $this->float()->notNull(),
            'y' => $this->float()->notNull(),
            'shape_id' => $this->integer()->notNull()
        ],'CHARSET utf8 COLLATE utf8_general_ci');
        $this->addForeignKey('fk_shape_point','point','shape_id','shape','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        echo "m190826_174010_add_point_table cannot be reverted.\n";
//
//        return false;
        $this->dropTable('shape');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190826_174010_add_point_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%shape}}`.
 */
class m190828_074733_add_points_column_to_shape_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shape', 'points', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('shape', 'points');
    }
}

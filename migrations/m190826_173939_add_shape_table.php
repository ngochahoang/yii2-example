<?php

use yii\db\Migration;

/**
 * Class m190826_173939_add_shape_table
 */
class m190826_173939_add_shape_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('shape', [
            'id' => $this->primaryKey(),
            'shape_name' => $this->string(100)->notNull(),
        ],'CHARSET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       // echo "m190826_173939_add_shape_table cannot be reverted.\n";
        $this->dropTable('shape');
       // return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190826_173939_add_shape_table cannot be reverted.\n";

        return false;
    }
    */
}

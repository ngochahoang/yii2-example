<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;

use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <div class="container-fluid">
        <?= $content ?>
    </div>
</div>



<?php $this->endBody() ?>
<script>
    var urlCreateShape = "<?php echo Yii::$app->request->baseUrl. '/shape/create' ?>";
    var urlCreatePoint = "<?php echo Yii::$app->request->baseUrl. '/point/create' ?>";
    var urlData = "<?php echo Yii::$app->request->baseUrl. '/draw/data' ?>";
    var urlGetMaxId = "<?php echo Yii::$app->request->baseUrl. '/draw/id' ?>";
    var urlUpdateShape = "<?php echo Yii::$app->request->baseUrl. '/shape/update' ?>";
    var urlDeleteShape = "<?php echo Yii::$app->request->baseUrl. '/shape/delete' ?>";
    var csrf = "<?=Yii::$app->request->getCsrfToken()?>";
    var urlIndex = "<?php echo Yii::$app->request->baseUrl. '/demo/index' ?>";
</script>
</body>
</html>
<?php $this->endPage() ?>

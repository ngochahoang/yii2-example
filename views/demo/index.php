<?php
use yii\helpers\html;
?>
    <div class="row">
        <?= Html::button('Cancel',['class' => 'btn btn-info'])?>
        <?= Html::button('Save',['class' => 'btn btn-success','disabled' => true])?>
    </div>
    <div class="row" style="margin:30px 0;">
        <?= Html::button('Upload Image',['class' => 'btn btn-light','id' => 'btn-upload-image'])?>
        <?= Html::button('Setting Construction',['class' => 'btn btn-light','disabled' => true])?>
        <?= Html::button('Choose Order for construction',['class' => 'btn btn-light','disabled' => true])?>
        <?= Html::button('Edit Construction',['class' => 'btn btn-light','disabled' => true])?>
        <?= Html::button('Finish',['class' => 'btn btn-light','disabled' => true])?>
    </div>
    <div id="main-container"></div>
    <img id="img" src="" alt="Image">
    <div id="upload-image-modal">
        <div class="modal-content">
            <div id="upload-image-title">Upload Image</div>
            <div style="display: flex;margin-top:10px;">
                <label>File: </label>
                <input type="text" name="path" style="width: 70%;margin-left:5px;">
                <input id='file' type='file' style="display: none;"/>
                <button id="btn-choose-file" class="btn btn-light">Choose</button>
            </div>
            <div style="margin-left:35px;">
                <div style="float:left;">
                    <p>Only upload .JPG file</p>
                    <p>Only upload file with 2MB</p>
                </div>
                <div style="float:right;">
                    <button id="btn-upload-file" class="btn btn-light">Upload</button>
                    <br>
                    <button id="btn-close" class="btn btn-light">Close</button>
                </div>
            </div>

        </div>
    </div>

</div>

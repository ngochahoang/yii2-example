<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>



<div class="d-flex flex-row my-2">
    <div class="col-12 d-flex justify-content-start">
        <button class="btn btn-success mx-3" id="btn-create">Create</button>
        <button class="btn btn-info mx-3" id="btn-edit">Edit</button>
        <button class="btn btn-warning mx-3" id="btn-save">Save</button>
    </div>
</div>
<div class="d-flex flex-row my-2" id="stage">
    <div class="col-12">
        <div id="container1"></div>
    </div>
</div>

<?php
$this->registerJsFile(Url::to(['js/custom.js']));
?>

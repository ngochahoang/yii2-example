function redirectPage(e){
    window.location.href = urlIndex;
    console.log(e);
}

$(function () {
    var width = window.innerWidth;
    var height = window.innerHeight;
    var files = '';

    var stage = new Konva.Stage({
        container: 'main-container',
        width: width,
        height: height
    });

    var layer = new Konva.Layer();
    stage.add(layer);




    $('#btn-upload-image').on('click',function () {

    });
    $('#btn-choose-file').on('click',function () {
        $('#file').click();

    });
    $('#file').on('change',function (event) {
        $("input[name=path]").val('');
        var fileName = event.target.files[0].name;
        var extension = fileName.split('.').pop();
        var fileSize = event.target.files[0].size;
        var isJPG = checkFileType(extension);
        var isValidSize = checkFileSize(fileSize);
        if(isJPG && isValidSize){
            $("input[name=path]").val(fileName);
        }
        files= event.target.files[0];


    });

    $('#btn-upload-file').on('click',function (e) {
        if($("input[name=path]").val()){
            var reader = new FileReader();
            reader.onload = function (evt) {
                $('#img').attr('src',evt.target.result);
            };
            reader.readAsDataURL(files);
            var URL = window.URL;
            var url = URL.createObjectURL(files);
            var imageObj = new Image();
            imageObj.src = url;
            // main API:

            imageObj.onload = function() {
                var yoda = new Konva.Image({
                    x: 0,
                    y: 0,
                    image: imageObj,
                    width: 300,
                    height: 300
                });

                // add the shape to the layer
                layer.add(yoda);
                layer.batchDraw();
            };
        }
    });


});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
function checkFileType(extension){
    var isJPG = true;
    if(extension != 'jpg'){
        isJPG=false;
    }
    return isJPG;
}
function checkFileSize(size){
    var isValid = true;
    if(size*Math.pow(10,-6) > 2){
        isValid=false;
    }
    return isValid;
}




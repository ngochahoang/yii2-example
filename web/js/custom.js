var shapeJSList = [];
var isCreate = false;
var width = window.innerWidth;
var height = window.innerHeight;
var stage = new Konva.Stage({
    container: 'container1',
    width: width,
    height: height
});
var stageWidth = stage.getWidth();
var stageHeight = stage.getHeight();
var container = stage.container();
container.tabIndex = 1;
container.focus();
var anchorLayer = new Konva.Layer();
var lineLayer = new Konva.Layer();
var currentShape =  "";
var updateAnchor = {};

$( document ).ready(function() {

  /*  $('#btn-edit').click(function(){
        isCreate = false;
        calculateUpdatePoint(updateAnchor,stageWidth,stageHeight);
     //   console.log(updateAnchor);
        if (Object.keys(updateAnchor).length <= 0) {
            return;
        }
        updateShape(updateAnchor);
    });*/


    $('#btn-create').click(function(){
        isCreate = true;
    });

    $('#btn-save').click(function(){
        console.log('Data');
        console.log(data);
        saveData(data);
        destroyInactiveAnchor();
        anchorLayer.draw();
    });

    var shapes = getData();// get all shape by constructionId
    console.log('Shapes = getData()');
    console.log(shapes);

    var data = {};

    //Draw shape if window resize
   $(window).resize(function(){
        var stageWidth = window.innerWidth;
        var stageHeight = window.innerHeight;
        stage.setWidth(stageWidth);
        stage.setHeight(stageHeight);
        shapeList = formatData(shapes);
        drawCurrentShape(shapeList);

    });

    var shapeList = formatData(shapes);
    drawCurrentShape(shapeList);

    lineLayer.on('click', function(e) {
        if(isCreate){
            return;
        }
        var shapeName = e.target.id();
        currentShape = shapeName;
        shapeId = shapeName.slice(5);//shape2-->2
      //  console.log(shapeList[shapeId]['points']);
        var points = formatPoint(shapeList[shapeId]['points']);
       // console.log(shapeList[shapeId]['points']);
        for(var i in points){
            var x = parseInt(points[i]['x']);
            var y = parseInt(points[i]['y']);
            //console.log(typeof x);
            shapeJSList[shapeName]['points'][i]=buildAnchor(x, y, shapeName,i);

        }
        anchorLayer.draw();

        destroyInactiveAnchor(currentShape);
        anchorLayer.draw();

     /*  container.onkeydown = function(evt){

           if(evt.keyCode == 46){
               //console.log(evt.keyCode);
               deleteShape(shapeId);
           }
       }*/
    });

    anchorLayer.on('beforeDraw', function(e) {
      updateDottedLines(currentShape);
    });

    stage.on('contentContextmenu',function(e) {
        e.evt.preventDefault();
    });

    var shapeId = getMaxShapeId()+1;// get max shapeId
    var newPoint = 0;
    var shapeIndex= 1;
    //var get number of areas in each construction
    // var numberOfArea = (ConstructionAreas->find->where('constructionId')->count())
    stage.on('click', function (e) {
        if (e.evt.button === 0){
            var stageWidth = stage.getWidth();
            var stageHeight = stage.getHeight();
            if(!isCreate)
                return;
            if(e.target instanceof Konva.Line){
                return;
            }
            if(e.target instanceof Konva.Circle){
                shapeId++;
                newPoint = 0;
            }else{
                var pos = stage.getPointerPosition();
                //shape1,shape2
                var shapeName = "shape"+shapeId;

                if(typeof shapeJSList[shapeName] === "undefined"){
                    shapeJSList[shapeName] = {"points" : {}};
                    shapeJSList[shapeName]['line'] = new Konva.Line({
                        points: [pos.x, pos.y],
                        fill: '#f7d8ff',
                        stroke: 'black',
                        strokeWidth: 3,
                        id: shapeName,
                        closed: true,
                    });
                    lineLayer.add(shapeJSList[shapeName]["line"]);

                }

                if(typeof data[shapeName] === 'undefined'){
                    data[shapeName] = [];
                    data[shapeName]['orderArea'] = shapeIndex++;
                    data[shapeName]['isUse'] = 'on';
                 //   data[shapeName]['orderArea'] = numberOfArea + shapeIndex++;
                    // id = createNewShape(shapeName);
                }
                // console.log('Stage:');
                // console.log(stageWidth);
                // console.log(stageHeight);
                // console.log('Position');
                // console.log(pos.x+'/'+pos.y);

                // var x = ((pos.x*100)/stageWidth).toFixed(3);
                // var y = ((pos.y*100)/stageHeight).toFixed(3);
                var x = calculatePoint(pos.x,stageWidth,3);
                var y = calculatePoint(pos.y,stageHeight,3);
                // console.log('Calculate');
                // console.log(x+'///'+y);
                data[shapeName].push({x: x, y: y});
                //save point by shapeId to point table


                var points = [];

                for (var pt in data[shapeName]){
                  //  console.log(data[shapeName][pt].x+'/'+data[shapeName][pt].y);
                    points.push(data[shapeName][pt].x);
                    points.push(data[shapeName][pt].y);
                }

                shapeJSList[shapeName]['points'][newPoint] = buildAnchor(pos.x, pos.y, shapeName,newPoint);
                newPoint++;
                anchorLayer.draw();

                updateDottedLines(shapeName);

            }
        }


    });

    stage.add(lineLayer);
    stage.add(anchorLayer);

});


function buildAnchor(x, y, shapeName,index) {
    var id = shapeName+index;
    var anchor = new Konva.Circle({
        x: x,
        y: y,
        radius: 15,
        stroke: '#66455c',
        fill: '#ddd',
        strokeWidth: 5,
        draggable: true,
        id:id,
    });
    anchor.on('mouseover', function() {
        currentShape = shapeName;
        document.body.style.cursor = 'pointer';
        this.strokeWidth(20);
        anchorLayer.draw();
    });
    anchor.on('mouseout', function() {
        document.body.style.cursor = 'default';
        this.strokeWidth(5);
        anchorLayer.draw();
    });
    updateAnchor['anchors'] = {};
    anchor.on('dragend', function(e) {
        updateDottedLines(currentShape);
        console.log('Dragend');
        console.log(currentShape);
        //Update only one shape once time
        updateAnchor['id'] = currentShape.slice(5);
        updateAnchor['anchors'][id] = newPositionAnchor(e.target.x(),e.target.y());

    });

    anchor.on('click', function (e) {
        if (e.evt.button === 2) {
            anchor.destroy();
        }
        return false;
    });

    anchorLayer.add(anchor);

    return anchor;
}

function newPositionAnchor(x,y){
    var anchor = [
        {
            x:x,
            y:y
        }
    ];
    return anchor;

}

function deleteShape(shapeId){
    $.ajax({
        type : 'post',
        url : urlDeleteShape,
        data : {
            id:shapeId,
            _csrf:csrf,
        },
        success: function(response) {
            if(response > 0){
                //console.log('Delete success');
                destroyInactiveAnchor();
                anchorLayer.draw();
                shapeJSList['shape'+shapeId]['line'].destroy();
                lineLayer.draw();
            }

        }


    });
}
//Format Json string get from db to array

function updateDottedLines(shapeName) {
    if(shapeName){
        var q = shapeJSList[shapeName]['points'];
        var quadLine = lineLayer.findOne("#"+shapeName);
        var points = [];
        for (var pt in q){
            points.push(q[pt].x());
            points.push(q[pt].y());
        }
        quadLine.points(points);
        lineLayer.draw();
    }
}

//get all data in shape table-> get all data from construction_areas
function getData(){
    var output = '';
    // passing constructionId by Ajax
    $.ajax({
        type : 'post',
        url : urlData,
        data : {
            _csrf:csrf,
        },
        async: false,
        success: function(response) {
            output = response;
            //   console.log(output);
        }


    });
    return output;
}

//Ajax save new shape to database
//send constructionId,orderNumber,isUse
function createNewShape(shapeName,points,orderArea,isUse){
    var output = '';
    console.log('Save data');
    console.log(shapeName);
    console.log(points);
    console.log(orderArea);
    console.log(isUse);
    $.ajax({
        type : 'post',
        url : urlCreateShape,
        dataType:'json',
        data : {
            shape_name:shapeName,
            points:points,
            _csrf:csrf,

        },
        async: false,
        success: function(response) {
            output = response;
            console.log(output);
        }


    });
    return output;
}

//Ajax function save db when user choose 'save' button
function saveData(data){

    for(var shapeName in data){
        // //Save shapename then save points by id
        // var id = createNewShape(shapeName);
        // console.log('Shape Index : '+shapeName+'-----');

        var points = '';
        var orderArea = data[shapeName]['orderArea'];
        var isUse = data[shapeName]['isUse'];
        for(var index in data[shapeName]){

                var x = data[shapeName][index].x;
                var y = data[shapeName][index].y;
                var prefix=',';
                // console.log(typeof  data[shapeName][index]);
                if(index == 0){
                    prefix ='';
                }
                points += prefix + Object.values(data[shapeName][index]);
                // console.log('['+ index +']'+' data x = '+x);
                // console.log(' data y ='+y);
                // console.log('-------');
                console.log('Points save = '+points);




        }

        //Save data
        createNewShape(shapeName,points,orderArea,isUse);
        // console.log('Points = '+points);
    }
    data = [];
}
//shapeList[id] = array[shapename,points];
function formatData(data){

    var array = JSON.parse(data);
    var data = {};
    var points = [];
    var pointArray = [];
    //console.log(shapeList);
    for(var i in array){
        pointArray[i] = [];
        points[i] = [];
        array[i]['points'] = array[i]['points'].split(',');
       // console.log(array[i]['points']);
        points[i] = formatPoint(array[i]['points']);
       // console.log(points[i]);
        for(var j in points[i]){
           // console.log(points[i][j]['x']+'--'+points[i][j]['y']);
          //  var x = (points[i][j]['x']*stageWidth)/100
            var x = parseFloat(points[i][j]['x']);
            var y = parseFloat(points[i][j]['y']);

            // points[i][j]['x'] = ((x*stageWidth)/100).toFixed(3);
            // points[i][j]['y'] = ((y*stageHeight)/100).toFixed(3);
            points[i][j]['x'] = calculatePointByRatio(x,stageWidth,3);
            points[i][j]['y'] = calculatePointByRatio(y,stageHeight,3);

            pointArray[i].push(points[i][j]['x'],points[i][j]['y']);

        }
        array[i]['points'] = pointArray[i];
      //  console.log(array[i]['points']);

        var id = array[i]['id'];
        //insert orderNumber and isUse
        data[id] = {
            shapeName : array[i]['shape_name'],
            points : array[i]['points']
        };
        //console.log(array[id]);

    }
   // console.log(data);
    return data;

}

function calculatePointByRatio(point,stage,fixDigit){
    return ((point*stage)/100).toFixed(fixDigit);
}

function calculatePoint(point,stage,fixDigit){
    return ((point*100)/stage).toFixed(3);
}

//draw current shape
//shapeList array of data get from db
//shapeJsList draw shapes
function drawCurrentShape(shapeList){
    lineLayer.destroyChildren();
    for (var id in shapeList){
        var shapeName = "shape"+id;
       // console.log(shapeList[id]['points']);
        shapeJSList[shapeName] = {"points" : {}};
        shapeJSList[shapeName]["line"] = new Konva.Line({
            points: shapeList[id]['points'],
            fill: '#ffcbf0',
            stroke: 'black',
            strokeWidth: 2,
            id: shapeName,
            closed: true,
            draggable: true,

        });
        lineLayer.add(shapeJSList[shapeName]["line"]);
    }
    //lineLayer.draw();
}

//format points
function formatPoint(data){
    var length = Object.keys(data).length;
    var points = [];
    for(var i = 1;i < length;i += 2){
        var point = {};
        point['x'] = data[i-1];
        point['y'] = data[i];
        points.push(point);

    }
    return points;
}

function getMaxShapeId(){
    var id = 0;
    $.ajax({
        type : 'post',
        url : urlGetMaxId,
        dataType:'json',
        data : {
            _csrf:csrf,
        },
        async: false,
        success: function(response) {
            id = response;
        }


    });
    return id;
}

function updateShape(data){
   // console.log(data['anchors']);
    $.ajax({
            type : 'post',
            url : urlUpdateShape,
            dataType:'json',
            data : {
                id : data['id'],
                anchors : JSON.stringify(data['anchors']),
                _csrf: csrf,
            },

            success: function(response) {
                if(response.result == 1){
                    destroyInactiveAnchor();
                    anchorLayer.draw();
                }

            }


        });
}

//After choosing another shape, it will destroy anchor point of
// the rest of shape not choose
function destroyInactiveAnchor(currentShape = ''){

    if (currentShape == " ") {
        for (var shapeName in shapeJSList) {
            for (var point in shapeJSList[shapeName]['points']) {
                shapeJSList[shapeName]["points"][point].destroy();
            }

        }
    } else {
        for (var shapeName in shapeJSList) {
            if (shapeName == currentShape) {
                continue;
            }
            for (var point in shapeJSList[shapeName]['points']) {
                shapeJSList[shapeName]["points"][point].destroy();
            }

        }
    }

}

function calculateUpdatePoint(updateAnchor,stageWidth,stageHeight){
    for(var i in updateAnchor['anchors']){
        for(var j in updateAnchor['anchors'][i]){
            var x = parseFloat(updateAnchor['anchors'][i][j]['x']);
            var y = parseFloat(updateAnchor['anchors'][i][j]['y']);
            //  console.log(x+'////'+y);
            updateAnchor['anchors'][i][j]['x'] = calculatePoint(x,stageWidth,3);
            updateAnchor['anchors'][i][j]['y'] = calculatePoint(y,stageHeight,3);
            //console.log('After');
            //console.log(calculatePoint(x,stageWidth,3)+'/////'+calculatePoint(y,stageHeight,3));
        }
    }
}



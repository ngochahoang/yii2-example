<?php


namespace app\models;

use yii\db\ActiveRecord;

class Shape extends  ActiveRecord
{


    public static function tableName()
    {
        return 'shape';
    }

    public function rules() {
        return [
            [['shape_name','points'], 'required'],
//

        ];
    }
    /**
     * @return array customized attribute labels
     */

    public function getPoints()
    {
        return $this->hasMany(Point::className(), ['shape_id' => 'id']);
    }




}
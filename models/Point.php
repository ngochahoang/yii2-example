<?php


namespace app\models;


use yii\base\Model;
use yii\db\ActiveRecord;


class Point extends ActiveRecord
{


    public static function tableName()
    {
        return 'point';
    }

    public function rules() {
        return [
            [['x', 'y', 'shape_id'], 'required'],

        ];
    }

    public function getShape()
    {
        return $this->hasOne(Shape::className(), ['id' => 'shape_id']);
    }
}
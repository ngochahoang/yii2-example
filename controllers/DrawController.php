<?php


namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\services\DrawService;
use yii\helpers\Json;
class DrawController extends Controller
{
    private $drawService;
    public function __construct($id, $module, $config = [],DrawService $drawService)
    {
        parent::__construct($id, $module, $config);
        $this->drawService = $drawService;
    }

    public function actionIndex(){
      //  Yii::info('Using index page');
        return $this->render('index');

    }

    public function actionData(){
        if(Yii::$app->request->isAjax) {
            $shapes = $this->drawService->getShapeData();
//            var_dump($shapes);
        //    Yii::info('Get data from database');
            return Json::encode($shapes);
        }
    }

    //get max id in table
    public function actionId(){
        if(Yii::$app->request->isAjax){
            $data = $this->drawService->getMaxShapeId(); // Object {"id":24,"shape_name":"shape0","points":"259,208,190,293,351,367,383,302"}
            return Json::encode($data['id']);
        }

    }

    


}
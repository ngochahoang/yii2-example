<?php


namespace app\controllers;


use yii\web\Controller;

class DemoController extends Controller
{
    public function actionRedirect(){

        return $this->render('redirect');
    }

    public function actionIndex(){
        return $this->render('index');
    }
}
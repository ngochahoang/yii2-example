<?php


namespace app\controllers;

use yii;
use app\models\Point;
use yii\web\Controller;
use app\services\PointService;

class PointController extends Controller
{
    private $pointService;
    public function __construct($id, $module, $config = [],PointService $pointService)
    {
        parent::__construct($id, $module, $config);
        $this->pointService=$pointService;
    }

    public function actionCreate(){
        if(Yii::$app->request->isAjax){
            $data = Yii::$app->request->post();
            $shape = $this->pointService->getShapeById(intval($data['shape_id']));
            $point = new Point();
            $point->x=$data['x'];
            $point->y=$data['y'];
            $point->link('shape', $shape);
            $point->save();

        }
    }
}
<?php


namespace app\controllers;

use app\services\ShapeService;
use Yii;
use app\models\Shape;
use yii\web\Controller;
use yii\helpers\Json;
use yii\web\Response;

class ShapeController extends Controller
{
    private $shapeService;
    public function __construct($id, $module, $config = [],ShapeService $shapeService)
    {
        parent::__construct($id, $module, $config);
        $this->shapeService=$shapeService;
    }

    //insert constructionId,orderNumber and isUser
    public function actionCreate(){
       if (Yii::$app->request->isAjax) {
           $data = Yii::$app->request->post();
           $model = new Shape();
           $values = [
                'shape_name' => $data['shape_name'],
                'points' => $data['points']
           ];
           $model->attributes = $values;
           $model->save();
         //  var_dump($result);
//            return Json::encode($data)
       }
   }

   public function actionUpdate(){
       if(Yii::$app->request->isAjax){
           $request = Yii::$app->request->post();
           $data = $this->shapeService->formatRequestAjax($request);
           $shape = Shape::findOne($data['id']);
           $pointString = $this->shapeService->handleUpdatePoints($shape,$data);
           $affectedRow = $shape->updateAttributes(['points' => $pointString]);
           if($affectedRow > 0)
               $response = ['result' => 1];
           else
               $response = ['result' => 0];

           return Json::encode($response);
       }
   }

   public function actionDelete(){
        if(Yii::$app->request->isAjax){
            $request = Yii::$app->request->post();
            $affectedRow = Shape::findOne($request['id'])->delete();
            if($affectedRow > 0)
                $result = 1 ;

            else
                $result = 0 ;
            return Json::encode($result);
        }
   }




}
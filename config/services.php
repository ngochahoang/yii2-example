<?php
return [
    'singletons' => [
        'drawService' => 'app\services\DrawService',
        'pointService' => 'app\services\PointService',
        'shapeService' => 'app\services\ShapeService',
    ],

];
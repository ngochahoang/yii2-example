<?php


namespace app\services;


use app\models\Shape;

class DrawService
{
    public function getShapeData(){
        $shapes = Shape::find()->all();
        return $shapes;
        //$data = ConstructionAreas::find()->where(['constructionId' => $constructionId])->all();

    }
    //get max Id
    public function getMaxShapeId(){
        $shape = Shape::find()->orderBy('id desc')->limit(1)->one();
        //$constructionAreas = ConstructionAreas::find()->orderBy('id desc')->limit(1)->one();
        return $shape;
    }
}
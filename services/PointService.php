<?php

namespace app\services;

use app\models\Shape;

class PointService

{
    public function getShapeById($id){
        $model = Shape::findOne($id);
        return $model;
    }
}
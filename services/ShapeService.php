<?php


namespace app\services;


class ShapeService
{
    public function formatRequestAjax($request){
        $data = [];
        $data['id'] = $request['id'];
        $data['anchors'] = [];
        $anchors = get_object_vars(json_decode($request['anchors']));
        foreach($anchors as $anchorId=>$arr){
            foreach($arr as $point){
                $data['anchors'][$anchorId] = get_object_vars($point);
            }
        }
        return $data;
    }

    public function formatPointsData($data){
        $array = explode(',',$data);
        $result = [];
        for($i=1;$i<count($array);$i+=2){
            $point = [
                  'x' => $array[$i-1],
                  'y' => $array[$i]
                ];
            array_push($result,$point);
        }
        return $result;

    }

    //Replace points was changed (data['anchors']) into original data
    // before update it to database
    public function replacePoints($points,$data){
        foreach($points as $k=>$value){
            $shapeName = 'shape'.$data['id'].$k;//shape . 9 . 0,1,2,....
            if(array_key_exists($shapeName,$data['anchors'])){
//                echo $shapeName;
                $points[$k]['x'] = $data['anchors'][$shapeName]['x'];
                $points[$k]['y'] = $data['anchors'][$shapeName]['y'];
            }

        }
        return $points;
    }

    //Convert points array to string before update it to database
    //@param points : array
    public function convertPointArray($points){
        $pointString = '';
        for($i=0;$i<count($points);$i++){
            $points[$i] = implode(',',array_values($points[$i]));
        }
        $pointString = implode(',',$points);
        return $pointString;

    }

    public function handleUpdatePoints($shape,$data){
        $points = $this->formatPointsData($shape['points']);
        // update points array after replace some points changed
        $points = $this->replacePoints($points,$data);
        //Convert points array to string before update it.
        $pointString = $this->convertPointArray($points);
        return $pointString;
    }
}